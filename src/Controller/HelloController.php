<?php

namespace App\Controller;

use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HelloController extends AbstractController {

    /**
     * @Route("/hello/{prenom<[a-zA-Z]*>?World}", "hello")
     */
    public function hello($prenom) 
    {
        return $this->render('hello.html.twig', ['prenom' => $prenom]);
    }
}